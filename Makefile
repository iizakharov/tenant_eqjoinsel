EXTENSION = tenant_eqjoinsel
DATA = $(wildcard sql/*.sql)
MODULES = $(patsubst %.c,%,$(wildcard src/*.c))

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
