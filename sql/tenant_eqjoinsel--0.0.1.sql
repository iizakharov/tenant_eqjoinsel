\echo Use "CREATE EXTENSION tenant_eqjoinsel" to load this file. \quit
CREATE FUNCTION tenant_eqjoinsel(
    internal,
    oid,
    internal,
    smallint,
    internal)
    RETURNS double precision
    AS '$libdir/tenant_eqjoinsel' LANGUAGE C STABLE STRICT;

CREATE FUNCTION tenant_neqjoinsel(
    internal,
    oid,
    internal,
    smallint,
    internal)
    RETURNS double precision
    AS '$libdir/tenant_eqjoinsel' LANGUAGE C STABLE STRICT;

--UPDATE pg_operator SET oprjoin = 'tenant_eqjoinsel'::regproc WHERE oprcode = 'uuid_eq'::regproc;
--UPDATE pg_operator SET oprjoin = 'tenant_neqjoinsel'::regproc WHERE oprcode = 'uuid_ne'::regproc;
