#include "postgres.h"
#include "fmgr.h"
#include "optimizer/clauses.h"
#include "optimizer/cost.h"
#include "optimizer/pathnode.h"
#include "utils/guc.h"
#include "utils/lsyscache.h"
#include "utils/selfuncs.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

#define TENANT_COLNAME "account_id"

extern void _PG_init(void);

static RelOptInfo *find_rel(PlannerInfo *root, Node* node);
static double tenant_selec(PlannerInfo *root, RelOptInfo *left_rel, RelOptInfo *right_rel);
static Node *tenant_clause(PlannerInfo *root, RelOptInfo *baserel);
static bool is_tenant_clause(PlannerInfo *root, Node *node);

static bool enabled = false;

void
_PG_init(void)
{
    static bool inited = false;

    if (inited)
        return;

    DefineCustomBoolVariable("tenant_eqjoinsel.enabled",
                             "Enable join selectivity correction",
                             NULL,
                             &enabled,
                             false,
                             PGC_USERSET, 0,
                             NULL, NULL, NULL);
}

PG_FUNCTION_INFO_V1(tenant_eqjoinsel);

Datum
tenant_eqjoinsel(PG_FUNCTION_ARGS)
{
    PlannerInfo *root = (PlannerInfo *) PG_GETARG_POINTER(0);
    Oid operator = PG_GETARG_OID(1);
    List *args = (List *) PG_GETARG_POINTER(2);
    JoinType jointype = (JoinType) PG_GETARG_INT16(3);
    SpecialJoinInfo *sjinfo = (SpecialJoinInfo *) PG_GETARG_POINTER(4);
    RelOptInfo *left_rel, *right_rel;
    float8 result;

    result = DatumGetFloat8(DirectFunctionCall5(eqjoinsel,
                                                PointerGetDatum(root),
                                                ObjectIdGetDatum(operator),
                                                PointerGetDatum(args),
                                                Int16GetDatum(jointype),
                                                PointerGetDatum(sjinfo)));

    if (enabled) {
        left_rel = find_rel(root, (Node *) linitial(args));
        right_rel = find_rel(root, (Node *) lsecond(args));

        if (left_rel && right_rel) {
            result /= tenant_selec(root, left_rel, right_rel);
            CLAMP_PROBABILITY(result);
        }
    }

    PG_RETURN_FLOAT8(result);
}

PG_FUNCTION_INFO_V1(tenant_neqjoinsel);

Datum
tenant_neqjoinsel(PG_FUNCTION_ARGS)
{
    PlannerInfo *root = (PlannerInfo *) PG_GETARG_POINTER(0);
    Oid operator = PG_GETARG_OID(1);
    List *args = (List *) PG_GETARG_POINTER(2);
    JoinType jointype = (JoinType) PG_GETARG_INT16(3);
    SpecialJoinInfo *sjinfo = (SpecialJoinInfo *) PG_GETARG_POINTER(4);
    Oid eqop;
    float8 result;

    eqop = get_negator(operator);
    if (eqop)
    {
        result = DatumGetFloat8(DirectFunctionCall5(tenant_eqjoinsel,
                                                    PointerGetDatum(root),
                                                    ObjectIdGetDatum(eqop),
                                                    PointerGetDatum(args),
                                                    Int16GetDatum(jointype),
                                                    PointerGetDatum(sjinfo)));
    }
    else
    {
        result = DEFAULT_EQ_SEL;
    }
    result = 1.0 - result;
    PG_RETURN_FLOAT8(result);
}

static RelOptInfo *
find_rel(PlannerInfo *root, Node *node)
{
    Node *basenode;

    if (IsA(node, RelabelType))
        basenode = (Node *) ((RelabelType *) node)->arg;
    else
        basenode = node;

    if (IsA(basenode, Var))
    {
        Var *var = (Var *) basenode;
        return find_base_rel(root, var->varno);
    }
    else
        return NULL;
}

static double
tenant_selec(PlannerInfo *root, RelOptInfo *left_rel, RelOptInfo *right_rel)
{
    if (left_rel->reloptkind == RELOPT_BASEREL && right_rel->reloptkind == RELOPT_BASEREL)
    {
        if (left_rel->rtekind == RTE_RELATION && right_rel->rtekind == RTE_RELATION)
        {
            Node *left_tenant_clause = tenant_clause(root, left_rel);
            Node *right_tenant_clause = tenant_clause(root, right_rel);
            if (left_tenant_clause && right_tenant_clause)
            {
                return clause_selectivity(root, right_tenant_clause, 0, JOIN_INNER, NULL);
            }
        }
    }
    return 1.0;
}

static Node *
tenant_clause(PlannerInfo *root, RelOptInfo *baserel)
{
    ListCell *l;
    foreach(l, baserel->baserestrictinfo)
    {
        RestrictInfo *rinfo = (RestrictInfo *) lfirst(l);
        Node *clause = (Node *) rinfo->clause;
        if (is_opclause(clause))
        {
            OpExpr *opclause = (OpExpr *) clause;
            if (list_length(opclause->args) == 2 && (
                is_tenant_clause(root, (Node *) linitial(opclause->args))
                || is_tenant_clause(root, (Node *) lsecond(opclause->args))))
                return (Node *) rinfo;
        }
    }
    return NULL;
}

static bool
is_tenant_clause(PlannerInfo *root, Node *node)
{
    Node *basenode;

    if (IsA(node, RelabelType))
        basenode = (Node *) ((RelabelType *) node)->arg;
    else
        basenode = node;

    if (IsA(basenode, Var))
    {
        Var *var = (Var *) basenode;
        RangeTblEntry* rte = NULL;
        if (var->varno < root->simple_rel_array_size)
            rte = root->simple_rte_array[var->varno];
        if (rte)
        {
            char *colname = get_relid_attribute_name(rte->relid, var->varattno);
            return strcmp(colname, TENANT_COLNAME) == 0;
        }
    }
    return false;
}
